package com.main.dspadmin;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
/**
 * Created by LEA on 6/14/2016.
 */
public class NewProductActivity extends Activity{
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputPrice;
    EditText inputStock;
    private static String url_create_product ="http://www.cranovation.com/php/create_barang.php";
    private static final String TAG_SUCCESS = "success";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("OUT", "Loaded");
        setContentView(R.layout.add_product);
        inputName = (EditText) findViewById(R.id.inputName);
        inputPrice = (EditText) findViewById(R.id.inputPrice);
        inputStock = (EditText) findViewById(R.id.inputStock);
        Button btnCreateProduct = (Button) findViewById(R.id.btnCreateProduct);
        btnCreateProduct.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String name, price, stock;
                name = inputName.getText().toString();
                price = inputPrice.getText().toString();
                stock = inputStock.getText().toString();
                new CreateNewProduct().execute(name,price,stock);
            }
        });
    }
    class CreateNewProduct extends AsyncTask<String, String, String>{
        String name, price, stock;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(NewProductActivity.this);
            pDialog.setMessage("Creating Product..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            String name = args[0];
            String price = args[1];
            String stock = args[2];
            params.put("name", name);
            params.put("price", price);
            params.put("stock", stock);
            JSONObject json = jsonParser.makeHttpRequest(url_create_product, "POST", params);
            Log.d("Create Response", json.toString());
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // success
                    Intent i = new Intent(getApplicationContext(), AllProductsActivity.class);
                    startActivity(i);
                    finish();
                } else {
                   //fail
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

    }
    protected void onPostExecute(String file_url) {
        pDialog.dismiss();
    }
}
