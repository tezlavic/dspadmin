package com.main.dspadmin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by LEA on 6/13/2016.
 */
public class EditProductActivity extends Activity {
    EditText txtName;
    EditText txtPrice;
    EditText txtDesc;
    EditText txtCreatedAt;
    Button btnSave;
    Button btnDelete;
    String pid;
    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    private final String url_product_details = getText(R.string.host)+"get_product_details.php";
    private final String url_update_product = getText(R.string.host)+"update_product.php";
    private final String url_delete_product =getText(R.string.host)+"delete_product.php";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "product";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
    private static final String TAG_PRICE = "price";
    private static final String TAG_STOCK = "stock";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_product);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        Intent i = getIntent();
        pid = i.getStringExtra(TAG_PID);
        new GetProductDetails().execute();

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String name, price, stock;
                name = txtName.getText().toString();
                price = txtPrice.getText().toString();
                stock = txtDesc.getText().toString();
                new SaveProductDetails().execute(name, price, stock);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                new DeleteProduct().execute();
            }
        });

    }

    class GetProductDetails extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog=new ProgressDialog(EditProductActivity.this);
            pDialog.setMessage("Loading product details. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        protected String doInBackground(String... params) {
            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        HashMap<String, String> params = new HashMap<>();
                        params.put("ID", pid);
                        JSONObject json = jsonParser.makeHttpRequest(
                                url_product_details, "GET", params);
                        Log.d("Detail barang", json.toString());

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {

                            JSONArray productObj = json
                                    .getJSONArray(TAG_PRODUCT);
                            JSONObject product = productObj.getJSONObject(0);
                            // product with this pid found
                            txtName = (EditText) findViewById(R.id.inputName);
                            txtPrice = (EditText) findViewById(R.id.inputPrice);
                            txtDesc = (EditText) findViewById(R.id.inputDesc);
                            //display
                            txtName.setText(product.getString(TAG_NAME));
                            txtPrice.setText(product.getString(TAG_PRICE));
                            txtDesc.setText(product.getString(TAG_STOCK));
                        }
                        else{

                        }
                        } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            return null;
        }
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }
    class SaveProductDetails extends AsyncTask<String, String, String> {
        String name, price, stock;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditProductActivity.this);
            pDialog.setMessage("Saving product ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        /*String name = args[0];
         String price = args[1];
         String stock = args[2];
         Save
         */

        protected String doInBackground(String... args) {

            HashMap<String, String> params = new HashMap<>();
            String name = args[0];
            String price = args[1];
            String stock = args[2];
            params.put("ID", pid);
            params.put("Nama", name);
            params.put("Harga", price);
            params.put("Stock", stock);
            JSONObject json = jsonParser.makeHttpRequest(url_update_product, "POST", params);
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                } else {
                    // fail
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }

    class DeleteProduct extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(EditProductActivity.this);
            pDialog.setMessage("Deleting Product...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        //DELETEEEEEE
        protected String doInBackground(String... args) {

            int success;
            try{
                HashMap<String, String> params=new HashMap<>();
                params.put("ID", pid);
                JSONObject json = jsonParser.makeHttpRequest( url_delete_product, "POST", params);
                Log.d("Delete Product", json.toString());
                success=json.getInt(TAG_SUCCESS);
                if(success==1){
                    Intent i=getIntent();
                    setResult(100,i);
                    finish();
                }
            }
            catch(JSONException e){
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

        }
    }



    }
