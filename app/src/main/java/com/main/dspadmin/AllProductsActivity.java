package com.main.dspadmin;
import android.app.Activity;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by LEA on 6/14/2016.
 */
public class AllProductsActivity extends ListActivity{
    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> productsList;
    JSONArray products = null;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "products";
    private static final String TAG_PID = "ID";
    private static final String TAG_NAME = "Nama";
    private static final String TAG_STOCK = "Stock";
    private static String url_all_products = "http://www.cranovation.com/php/get_all_products.php";
    //MUST MAKE AUTHENTIFICATION PROTOCOL LATER
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("OUT", "Loaded");
        setContentView(R.layout.all_products);
        productsList = new ArrayList<HashMap<String, String>>();
        new LoadAllProducts().execute();
        ListView lv = getListView();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            //result code 100: user del/edit product, so reload again
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

    }
    class LoadAllProducts extends AsyncTask<String, String, String> {
        protected void onPreExecute(){
            super.onPreExecute();
            pDialog = new ProgressDialog(AllProductsActivity.this);
            pDialog.setMessage("Loading products. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }
        protected String doInBackground(String... args) {
            HashMap<String, String> params = new HashMap<>();
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);
            Log.d("All Products: ", json.toString());
            try{
                int success = json.getInt(TAG_SUCCESS);
                if (success == 1) {
                    products = json.getJSONArray(TAG_PRODUCTS);
                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);
                        String id = c.getString(TAG_PID);
                        String name = c.getString(TAG_NAME);
                        String stok = c.getString(TAG_STOCK);
                        //Insert result ke hashmap
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_PID, id);
                        map.put(TAG_NAME, name);
                        map.put(TAG_STOCK, stok);
                        productsList.add(map);
                    }
                }
                else{
                    Intent i = new Intent(getApplicationContext(), NewProductActivity.class);
                    startActivity(i);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
                return null;
        }
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ListAdapter adapter = new SimpleAdapter(AllProductsActivity.this, productsList, R.layout.list_item, new String[]{TAG_PID, TAG_NAME, TAG_STOCK}, new int[]{R.id.pid, R.id.name, R.id.stock});
                    setListAdapter(adapter);
                }
            });
        }

    }
}
